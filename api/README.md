# Claimz API

## Description

API for communication between the [webapp](../webapp/) and [peppermint](../peppermint/).

Built with [Nest](https://github.com/nestjs/nest).

## Installation

```bash
$ yarn install
```

## Running the app

```bash
# development
$ yarn run start

# watch mode
$ yarn run start:dev

# production mode
$ yarn run start:prod
```

## Test

```bash
# unit tests
$ yarn run test

# e2e tests
$ yarn run test:e2e

# test coverage
$ yarn run test:cov
```

## Licenses

Claimz is [MIT licensed](../LICENSE).

Nest is [MIT licensed](https://github.com/nestjs/nest/blob/master/LICENSE).
