import {
  Entity,
  Column,
  PrimaryGeneratedColumn,
  CreateDateColumn,
  UpdateDateColumn,
} from 'typeorm';

interface CommandArgs {
  token_id: number;
  to_address: string;
  metadata_ipfs: string;
  amount: number;
}

interface Command {
  handler: string;
  name: string;
  args: CommandArgs;
}

@Entity({ schema: 'peppermint' })
export class Operations {
  @PrimaryGeneratedColumn()
  id: number;

  @CreateDateColumn({ name: 'submitted_at' })
  submittedAt: Date;

  @Column()
  originator: string;

  @Column()
  state: string;

  @Column('jsonb', { nullable: false, default: {} })
  command: string;

  @Column({ name: 'included_in', nullable: true })
  includedIn: string;

  @UpdateDateColumn({ name: 'last_updated_at' })
  lastUpdatedAt: Date;
}
