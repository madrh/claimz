import { Injectable } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { TransferDto } from './dto/transfer.dto';
import { Operations } from './operations.entity';

@Injectable()
export class OperationsService {
  constructor(private configService: ConfigService) {}

  @InjectRepository(Operations)
  private readonly repository: Repository<Operations>;

  public transfer(body: TransferDto): Promise<any> {
    const transferTez: Operations = new Operations();
    transferTez.submittedAt = new Date();
    transferTez.originator = this.configService.get<string>('originator');
    transferTez.state = 'pending';
    transferTez.command = JSON.stringify({
      handler: 'tez',
      name: 'transfer',
      args: {
        to_address: body.to_address,
        amount: 0.01,
      },
    });
    transferTez.includedIn = null;
    transferTez.lastUpdatedAt = new Date();

    const transferNft: Operations = new Operations();
    transferNft.submittedAt = new Date();
    transferNft.originator = this.configService.get<string>('originator');
    transferNft.state = 'pending';
    transferNft.command = JSON.stringify({
      handler: 'nft',
      name: 'transfer',
      args: {
        token_id: 0,
        from_address: this.configService.get<string>('originator'),
        to_address: body.to_address,
        amount: 1,
      },
    });
    transferNft.includedIn = null;
    transferNft.lastUpdatedAt = new Date();

    return this.repository.save([transferTez, transferNft]);
  }
}
