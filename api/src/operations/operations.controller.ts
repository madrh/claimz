import { Body, Controller, Delete, Get, Param, Post } from '@nestjs/common';
import { OperationsService } from './operations.service';
import { TransferDto } from './dto/transfer.dto';

@Controller('operations')
export class OperationsController {
  constructor(private readonly operationsService: OperationsService) {}

  @Post()
  transfer(@Body() transferDto: TransferDto): Promise<any> {
    return this.operationsService.transfer(transferDto);
  }
}
