import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { OperationsController } from './operations.controller';
import { OperationsService } from './operations.service';
import { Operations } from './operations.entity';

@Module({
  imports: [TypeOrmModule.forFeature([Operations])],
  controllers: [OperationsController],
  providers: [OperationsService],
})
export class OperationsModule {}
