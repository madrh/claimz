export default () => ({
  port: parseInt(process.env.PORT, 10) || 3000,
  originator: process.env.ORIGINATOR || '',
  database: {
    host: process.env.POSTGRES_HOST || '127.0.0.1',
    port: parseInt(process.env.DATABASE_PORT, 10) || 5432,
    username: process.env.POSTGRES_USER || 'tezos',
    password: process.env.POSTGRES_PASSWORD || 'tezos',
    name: process.env.POSTGRES_DATABASE || 'tezos',
  },
});
