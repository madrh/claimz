init:
	docker compose up -d db hasura

start:
	docker compose up -d

stop:
	docker compose down

clear:
	docker compose down --volumes
