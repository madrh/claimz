# Claimz

Claimz allows nft giveaways to be distributed through an interface without users paying gas fees.

## Installation

### Docker (recommended)

You must configure the environment file and the Peppermint config before launching the containers.

#### Environment

Copy the file `env.example` into `.env` and edit it.

It is mainly necessary to modify the following variables:

- `API_ORIGINATOR`: This is the address of the person who will send the NFTs and pay for the gas
- `NEXT_PUBLIC_CONTRACT`: The address of the NFT contract

#### Peppermint config

Copy the file `config.example.json` into `config.json` and edit it.

- `privateKey`: The secret key corresponding to the address of `API_ORIGINATOR` in the environment file
- `handlers.nft.args.contract_address`: The address of the NFT contract

#### Setting up

Once the configuration files are ready, launch the containers with the following command:

```bash
docker compose up -d
```

### From scratch

#### Peppermint

You first need to configure Peppermint.

Go in the `peppermint` folder and modify the `config.json` file.

You mainly need to modify the following variables:

- `privateKey`: The private key **unencrypted** of the holder of NFTs (the account that will send the NFTs and the Tezzies);

- `rpcUrl`: The url of a node;

- `contract_address`: The address of the NFT contract.

#### API

The second step is to configure the API.

Go in the `api` folder, and copy the `env.example` to `.env`.

You mainly need to modify the following variables:

- `ORIGINATOR`: The Tezos public hash key (tz1, tz2...) corresponding to the private key put in the `config.json` of Peppermint.

#### Webapp

Go in the `webapp` folder, and copy the `env.example` to `.env`.

You mainly need to modify the following variables:

- `NEXT_PUBLIC_CONTRACT`: The address of the NFT contract;

- `NEXT_PUBLIC_TOKENID`: The id of the token that will be send;

## Usage

By default, you can access:

- the webapp on the port 3000
- the API on the port 5000

The API contains only one route:

- POST /operations: { to_address: ADDRESS_TO_SEND_NFT }

## Contributing
Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.

## License
[MIT](https://choosealicense.com/licenses/mit/)